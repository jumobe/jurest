#!/usr/bin/env bash

rm -rf CMakeCache.txt CMakeFiles/ Makefile cmake_install.* build/

mkdir build

cd build

cmake .. -DCMAKE_CXX_COMPILER=$(which g++)

#include <csignal>
#include <iostream>
#include <memory>
#include "server.h"

std::unique_ptr<jurest::server> server;

void signal_handler(int sig)
{
  std::cerr << "Signal received: ["
            << sig
            << "]"
            << std::endl;
  if(server != nullptr)
  {
    server->stop();
  }
}

int main()
{
  auto a = [](jurest::request) { return "test-api"; };
  jurest::request_action ra;
  ra.add_action("/api", jurest::http_method::GET, a);
  
  server = std::make_unique<jurest::server>();
  std::signal(SIGINT, signal_handler);
  server->request_action(ra)->start();
  return 0;
}

// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:

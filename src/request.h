#ifndef _CLASS_REQUEST_H_
#define _CLASS_REQUEST_H_

#include <map>
#include <string>
#include <vector>

namespace jurest {
  enum http_method { OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE, TRACE, CONNECT };
  
  class request
  {
  public:
    explicit request(const std::string);
    ~request();

  private:
    std::string raw_headers;
    std::string raw_body;
    std::vector<std::string> raw_elements;
    jurest::http_method method;
    std::string uri;
    std::string protocol;
    std::map<std::string, std::string> headers;
    std::map<std::string, std::string> query_params;

    std::vector<std::string> decode_raw() const;
    void uri_interpreter(const std::string);
    jurest::http_method method_for_name(const std::string) const;

    friend class process_request;
    friend class request_action;
  };
}
#endif

// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:

#ifndef _CLASS_SERVER_H_
#define _CLASS_SERVER_H_

#include <string>

#include "request_action.h"

namespace jurest {
  class server
  {
  public:
    server();
    ~server();
    void start();
    void stop();
    jurest::server *request_action(const jurest::request_action);

  private:
    int sockfd;
    bool accept_loop;
    unsigned int port;
    jurest::request_action ra;
    
    void exec_request_action(const unsigned int,
                             const std::string) const;
  };
}
#endif

// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:

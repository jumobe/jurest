#include "request.h"

#include <iostream>
#include <memory>
#include <regex>
#include <string>

jurest::request::request(const std::string buff)
{
  const size_t body_init = buff.find("\r\n\r\n");

  if(body_init == std::string::npos)
  {
    throw std::runtime_error("Error to find request body: " + buff);
  }

  this->raw_headers = buff.substr(0, body_init);
  this->raw_body = buff.substr(body_init + 4);

  this->raw_elements = this->decode_raw();

  // request line
  const std::regex req_line_regex("([A-Z]+) (\\/.*) (HTTP\\/[0-9]+\\.[0-9]+)");
  // header regex
  const std::regex header_regex("(.+): (.+)");

  std::smatch base_match;
  for(const auto p: this->raw_elements)
  {
    if (std::regex_match(p, base_match, req_line_regex)
        && (base_match.size() == 4))
    {
      this->method = this->method_for_name(base_match.str(1));
      this->uri = base_match.str(2);
      this->uri_interpreter(base_match.str(2));
      this->protocol = base_match.str(3);
    }
    else if (std::regex_match(p, base_match, header_regex)
             && (base_match.size() == 3))
    {
      this->headers[base_match.str(1)] = base_match.str(2);
    }
  }
}

std::vector<std::string>
jurest::request::decode_raw() const
{
  std::vector<std::string> pts_request;
  auto not_empty_str = [](auto c){ return !std::isblank(c); };
  std::string pts_r = this->raw_headers;
  size_t delimiter_pos = 0;
  std::string pt_r;

  do
  {
    pts_r = pts_r.substr(delimiter_pos);
    delimiter_pos = pts_r.find("\r\n");
    if(delimiter_pos != std::string::npos)
    {
      pt_r = pts_r.substr(0, delimiter_pos);
      delimiter_pos += 2;
    }
    else
    {
      pt_r = pts_r;
    }
    if(std::count_if(pt_r.begin(), pt_r.end(), not_empty_str) > 0)
    {
      pts_request.push_back(pt_r);
    }
  } while (delimiter_pos != std::string::npos);
  return pts_request;
}

void
jurest::request::uri_interpreter(const std::string uri)
{
  size_t query_delimiter = uri.find("?");
  if(query_delimiter == std::string::npos)
  {
    this->uri = uri;
  }
  else
  {
    this->uri = uri.substr(0, query_delimiter);
    const std::regex query_regex("(.+)=(.+)");
    std::smatch base_match;
    std::string substr = uri.substr(query_delimiter + 1);
    do
    {
      query_delimiter = substr.find("&");
      std::string pt = substr;

      if(query_delimiter != std::string::npos)
      {
        pt = substr.substr(0, query_delimiter);
        substr = substr.substr(query_delimiter + 1);
      }

      if(std::regex_match(pt, base_match, query_regex)
         && (base_match.size() == 3))
      {
        this->query_params[base_match.str(1)] = base_match.str(2);
      }
    } while( query_delimiter != std::string::npos);
  }
}

jurest::http_method
jurest::request::method_for_name(const std::string mstr) const
{
  std::map<std::string, jurest::http_method> dic_method;
  dic_method["GET"] = jurest::http_method::GET;
  dic_method["POST"] = jurest::http_method::POST;
  dic_method["PUT"] = jurest::http_method::PUT;
  dic_method["PATCH"] = jurest::http_method::PATCH;
  dic_method["DELETE"] = jurest::http_method::DELETE;

  return dic_method[mstr];
}

jurest::request::~request()
{
}

// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:

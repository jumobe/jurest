#include "server.h"
#include "request.h"
#include "process_request.h"

#include <cstring>
#include <iostream>
#include <thread>

#include <netinet/in.h>
#include <csignal>
#include <sys/socket.h>
#include <unistd.h>

jurest::server::server()
{
  this->port = 9292;
  std::cout << "Start with default port ["
            << this->port
            << "]"
            << std::endl;
}

void
jurest::server::start()
{
  const unsigned int buffsz = 1 << 16;
  char buff[buffsz];
  struct sockaddr_in serv_addr;

  this->sockfd = socket(AF_INET, SOCK_STREAM, 0);

  if(this->sockfd < 0)
  {
    throw std::runtime_error("Error call socket()");
  }

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(this->port);

  if( bind(this->sockfd,
           (struct sockaddr *) &serv_addr,
           sizeof(serv_addr)) != 0)
  {
    throw std::runtime_error("Error call bind()");
  }

  if( listen(this->sockfd, 50) != 0)
  {
    throw std::runtime_error("Error call listen()");
  }

  this->accept_loop = true;
  do
  {
    memset(buff, 0, buffsz);
    int newsockfd = accept(this->sockfd, NULL, NULL);
    memset(buff, 0, buffsz);

    unsigned long n = recv(newsockfd, buff, buffsz, 0);

    std::cout << "--- INIT ------ [" << n << "]" << std::endl;
    if(n > 0)
    {
      std::thread t([this, newsockfd, buff, n](){
                      this->exec_request_action(newsockfd,
                                                std::string(buff, n));});
      t.detach();
      //t.join();
    }
    std::cout << "--- FINISH ------" << std::endl;
  } while(this->accept_loop);
  close(this->sockfd);
}

void
jurest::server::exec_request_action(const unsigned int fd,
                                    const std::string buff) const
{
  jurest::request req(buff);
  jurest::process_request pr(fd, req, this->ra);
}

jurest::server *
jurest::server::request_action(const jurest::request_action ra)
{
  this->ra = ra;
  return this;
}

void
jurest::server::stop()
{
  this->accept_loop = false;
  if( close(this->sockfd) != 0 )
  {
    throw std::runtime_error("Error call close() main fd");
  }
}

jurest::server::~server()
{
}

// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:

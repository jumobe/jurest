#ifndef _CLASS_PROCESS_REQUEST_H_
#define _CLASS_PROCESS_REQUEST_H_

#include <memory>

#include "request.h"
#include "request_action.h"

namespace jurest {
  class process_request
  {
  public:
    explicit process_request(const unsigned int,
                             const request,
                             const jurest::request_action);
    ~process_request();

  private:
    std::string files_path;
    
    const std::string favicon_response() const;
    const std::string file_response(const std::string) const;
  };
}
#endif

// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:

#ifndef _CLASS_REQUEST_ACTION_H_
#define _CLASS_REQUEST_ACTION_H_

#include <functional>
#include <map>
#include <string>
#include <tuple>
#include <vector>

#include "request.h"

namespace jurest {
  typedef std::function<std::string(jurest::request)> action_executor;
  typedef std::tuple<std::string,
                     jurest::http_method,
                     jurest::action_executor> map_action;
  
  class request_action
  {
  public:
    
    explicit request_action();
    ~request_action();
    request_action *add_action(const std::string,
                               const jurest::http_method,
                               const jurest::action_executor);
    std::string static_files_loc();
    jurest::action_executor get_action(const jurest::request&) const;
    void static_files_loc(const std::string);

  private:
    std::string static_files_path;
    std::vector<jurest::map_action> actions;
  };
}
#endif

// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:

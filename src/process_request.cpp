#include "process_request.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <sys/socket.h>
#include <unistd.h>

jurest::process_request::process_request(const unsigned int fd,
                                         const request req,
                                         const jurest::request_action ra)
{
  const std::vector<std::string> request = req.raw_elements;
  auto print = [](auto s){ std::cout << "[" << s << "]" << std::endl;};
  std::for_each(request.begin(), request.end(), print);

  this->files_path = ".";

  std::string msg;

  auto action = ra.get_action(req);
  
  if (action != nullptr)
  {
    msg = action(req);
  } else if(req.uri == "/favicon.ico")
  {
    msg = favicon_response();
  }
  else if(req.uri.find(".html") != std::string::npos)
  {
    msg = this->file_response(req.uri);
  }
  else
  {
    msg = "HTTP/1.1 200 OK\r\n"
      "Server: jurest\r\n"
      "Content-type: text/html\r\n\r\n"
      "<html><body><h1>juRest</h1></body></html>"
      "\r\n";
  }

  send(fd, msg.c_str(), msg.length(), 0);
  close(fd);
}

const std::string
jurest::process_request::favicon_response() const
{
  std::stringstream ss;
  ss << "HTTP/1.1 200 OK \r\n";
  ss << "Server: jurest\r\n";
  std::fstream fi("favicon.ico",
                  std::ios::binary | std::ios::in | std::ios::ate);
  if(fi)
  {
    unsigned int fi_size = fi.tellg();
    fi.seekg(0, std::ios::beg);
    char *buff = new char[fi_size];
    ss << "Content-Type: image/x-icon\r\n";
    ss << "Content-Length: " << fi_size << "\r\n\r\n";
    fi.read(buff, fi_size);
    fi.close();
    for(unsigned int i = 0; i < fi_size; i++)
    {
      ss << buff[i];
    }
    delete(buff);
    ss << "\r\n";
  }
  return ss.str();
}

const std::string
jurest::process_request::file_response(const std::string path) const
{
  std::stringstream ss;
  ss << "HTTP/1.1 200 OK \r\n";
  ss << "Server: jurest\r\n";
  ss << "Content-Type: text/html\r\n\r\n";
  std::fstream fi(this->files_path + path, std::ios::in);
  if(fi)
  {
    std::string line;
    while(std::getline(fi, line))
    {
      ss << line;
    }
    fi.close();
    ss << "\r\n";
  }
  return ss.str();
}

jurest::process_request::~process_request()
{
}

// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:

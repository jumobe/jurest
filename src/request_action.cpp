#include "request_action.h"

jurest::request_action::request_action()
{
  this->static_files_path = "./";
}

jurest::request_action*
jurest::request_action::add_action(const std::string uri,
                                   const jurest::http_method method,
                                   const jurest::action_executor act)
{
  this->actions.push_back(std::make_tuple(uri, method, act));
  return this;
}

void
jurest::request_action::static_files_loc(const std::string loc)
{
  this->static_files_path = loc;
}

std::string
jurest::request_action::static_files_loc()
{
  return this->static_files_path;
}

jurest::action_executor
jurest::request_action::get_action(const jurest::request& req) const
{
  for(auto &a: this->actions)
  {
    if( (std::get<0>(a) == req.uri) && (std::get<1>(a) == req.method))
    {
      return std::get<2>(a);
    }
  }
  return nullptr;
}

jurest::request_action::~request_action()
{
}
  
// Local Variables:
// mode: c++
// c-file-style: "linux"
// c-basic-offset: 2
// indent-tabs-mode: nil
// End:
